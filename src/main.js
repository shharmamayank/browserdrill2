const screen = document.getElementById("window");
function renderData(todos, userData) {
    userData.map((obj) => {
        let getId = obj.id;
        let userName = userData.filter(data => { return data.id == getId }).map(data => { return data.name })
        // Element creation
        const div = document.createElement("div");
        const h1 = document.createElement("h1");
        const headText = document.createTextNode(obj.name);
        div.appendChild(h1);

        todos.map((element) => {
            let todos = element.slice(0, 7);
            todos.map((todo) => {
                if (todo.userId === getId) {

                    div.innerHTML += `<div class="container">
                    <input class=check-box type="checkbox">
                    <h3> @ ${userName} : ${todo.title}</h3>
                    </div>`
                }
            });
        });
        screen.appendChild(div);
    });
}
