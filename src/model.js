function getUsers(api) {
  return fetch(api).then(data => {
    return data.json()
  })
    .catch((error) => {
      console.log(error)
    })
}
getUsers("https://jsonplaceholder.typicode.com/users").then((userData) => {
  return userData.map(data => {
    return (data.id)
  })
}).then(userData=>{
 return Promise.all([userData,userData.map(data => {
  return `https://jsonplaceholder.typicode.com/todos?userId=${data}`
})])
}).then(urlData=>{
  // console.log(urlData)
 return Promise.all(
   [urlData[0],
   Promise.all(urlData[1].map((url) => fetch(url)))]
  )
  
}).then(urlData=>{
 return Promise.all(
   [urlData[0],
   Promise.all(urlData[1].map((obj) => obj.json()))]
  )  
}).then((data) => {
      renderData(data[1], data[0]);
    })
    .catch((error) => {
      console.log(error);
    });


